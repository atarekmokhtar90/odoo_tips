# -*- coding: utf-8 -*-
{
    'name': "Load owl template from xml in frontend",

    'summary': """
        Load owl template from xml in frontend""",

    'description': """
        Load owl template from xml in frontend
    """,

    'author': "Ahmed Mokhtar",

    # for the full list
    'category': 'Website',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'web',
        'website',
    ],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/simple_page.xml',
    ],
    'assets': {
        'web.assets_frontend': [
            'load_owl_template_from_xml_frontend/static/src/component/simple_component.js',
            'load_owl_template_from_xml_frontend/static/src/utils/utils.js',
        ],
    },
}
