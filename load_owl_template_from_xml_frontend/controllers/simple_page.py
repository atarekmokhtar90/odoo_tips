# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import http, _
from odoo.http import request


class SimplePage(http.Controller):

    @http.route('/simple_page', type='http', auth="public", website=True)
    def portal_my_invoice_detail(self):
        return request.render("load_owl_template_from_xml_frontend.simple_page")
