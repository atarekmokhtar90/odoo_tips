/** @odoo-module **/

import { load_owl_template_from_xml } from "../utils/utils.js";
const { Component, mount } = owl;
const { whenReady } = owl.utils;
const { useState } = owl.hooks;
const template_path = "/load_owl_template_from_xml_frontend/static/src/component/simple_component_view.xml";

class SimpleComponent extends Component {
    setup(){
        this.props = useState({content: ""})
    }

    onChangeInputText(e){
        this.props.content = e.target.value;
    }
}

async function setup() {
    await load_owl_template_from_xml(
        template_path,
        "SimpleComponent",
        SimpleComponent
    )
    mount(SimpleComponent, { target: document.getElementById("simple_page_body")});
}

whenReady(setup);
