/** @odoo-module **/

const { loadFile } = owl.utils;
const { xml } = owl.tags;

export async function load_owl_template_from_xml(file_path, template_name, component){

    let templates = await loadFile(file_path);
    const doc = new DOMParser().parseFromString(templates, "text/xml");
    let query = `templates > t[t-name="${template_name}"][owl]`;
    if (doc.querySelector(query) == null){
        component.template = xml/*xml*/`<h1>Template ${template_name} could not be loaded ..</h1>`;
        return
    }
    let temp_body = doc.querySelector(query).innerHTML;
    component.template = xml/*xml*/`${temp_body}`;
}